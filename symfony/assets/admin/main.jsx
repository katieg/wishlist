'use strict';

import './css/style.scss';

import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store/store';
import Index from './pages/Index.jsx';

render(
    <Provider store={store}>
        <BrowserRouter>
            <div className="page">
                <div className="content">
                    <Route exact path="/admin" component={Index} />
                </div>
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('app')
);