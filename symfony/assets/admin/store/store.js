'use strict';

import {createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';

import exampleReducer from './reducers/example'

let reducers = combineReducers({
    exampleReducer
});

export default createStore(reducers, applyMiddleware(thunkMiddleware));