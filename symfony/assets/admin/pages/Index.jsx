'use strict';

import React from 'react';

const Index = () => {
    return (
        <div className="page-index">
            I'm an admin page.
        </div>
    );
};

export default Index;