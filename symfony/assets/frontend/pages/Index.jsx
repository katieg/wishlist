'use strict';

import React from 'react';

const Index = () => {
    return (
        <div className="page-index">
            I'm a frontend page.
        </div>
    );
};

export default Index;