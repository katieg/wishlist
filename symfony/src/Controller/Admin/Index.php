<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class Index extends Controller {
	/**
	 * @Route("/admin")
	 */
	public function example() {
		return $this->render('admin.html.twig', []);;
	}
}