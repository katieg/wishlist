<?php

namespace App\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class Index extends Controller {
	/**
	 * @Route("/")
	 */
	public function example(Request $request) {
		return $this->render('frontend.html.twig', [
			'app_host' => $request->getHost()
		]);
	}
}