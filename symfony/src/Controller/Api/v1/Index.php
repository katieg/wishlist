<?php

namespace App\Controller\Api\v1;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class Index extends Controller {
	/**
	 * @Route("/api/v1")
	 * @Method({"GET"})
	 */
	public function example() {
		return $this->json(array('username' => 'jane.doe'));
	}
}