import React from 'react';
import {shallow} from 'enzyme';
import Index from '../assets/frontend/pages/Index.jsx';

test('Index has I\'m a frontend page. text', () => {
    // Render a checkbox with label in the document
    const index = shallow(<Index />);

    expect(index.find('div').text()).toEqual('I\'m a frontend page.');
});