<?php
$I = new ApiTester($scenario);
$I->wantTo('get the index and see the result');
$I->sendGET('');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
$I->seeResponseIsJson();
$I->seeResponseContains('{"username":"jane.doe"}');
