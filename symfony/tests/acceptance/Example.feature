Feature: Example
  In order to test acceptance tests
  As a developer
  I need to view the frontend Example page

  Scenario: view frontend Example page
    When I go to /
    Then I see a page with "I'm a frontend page.".
